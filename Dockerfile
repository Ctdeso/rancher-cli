FROM alpine
RUN apk add --no-cache ca-certificates openssh-client curl jq
WORKDIR /usr/bin
RUN curl -s https://api.github.com/repos/rancher/cli/releases/latest | \
	jq '.assets[].browser_download_url | select(.|test(".+linux-amd64.+xz"))' -r | \
	xargs -n1 curl -Ls  | \
	tar -Jxxvf - --strip=2
RUN curl -LOs https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
	chmod +x /usr/bin/kubectl
WORKDIR /mnt
